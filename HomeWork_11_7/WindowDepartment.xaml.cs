﻿using HomeWork_11_7.Data;
using HomeWork_11_7.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HomeWork_11_7
{
    /// <summary>
    /// Interaction logic for WindowDepartment.xaml
    /// </summary>
    public partial class WindowDepartment : Window
    {
        private WindowMain parent;
        private Department? department;
        public WindowDepartment(WindowMain parent, Department? department = null)
        {
            InitializeComponent();
            this.parent = parent;
            this.department = department;
        }        

        private void ButtonAction_Click(object sender, RoutedEventArgs e)
        {
            string name = TextBoxName.Text;

            if (name != "")
            {
                if (department != null)
                {
                    (parent.User as Manager).UpdateDepartment(department.Id, name);                    
                } else
                {
                    (parent.User as Manager).AddDepartment(name);
                }                
                Close();
            } else
            {
                MessageBox.Show("Name cannot be empty");
            }            
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            if (department == null)
            {
                Title = "Add depatment";
                ButtonAction.Content = "Add";
            }
            else
            {
                Title = "Edit department";
                ButtonAction.Content = "Save";
                TextBoxName.Text = department.Name;
            }
        }
    }
}
