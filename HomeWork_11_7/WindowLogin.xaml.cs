﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HomeWork_11_7
{
    /// <summary>
    /// Interaction logic for WindowLogin.xaml
    /// </summary>
    public partial class WindowLogin : Window
    {
        private WindowMain parent;
        public WindowLogin(WindowMain parent)
        {
            InitializeComponent();
            this.parent = parent;
        }

        private void ButtonConsultant_Click(object sender, RoutedEventArgs e)
        {
            parent.Mode = WindowMain.ModeConsultant;
            Close();
        }

        private void ButtonManager_Click(object sender, RoutedEventArgs e)
        {
            parent.Mode = WindowMain.ModeManager;
            Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (parent.Mode == 0)
            {
                Application.Current.Shutdown();
            }
        }
    }
}
