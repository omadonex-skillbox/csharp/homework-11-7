﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_11_7.Data
{
    public class Department : FillableRecord
    {
        [JsonProperty("name")]
        private string name;
        [JsonIgnore]
        public string Name { get { return name; } set { name = value; NotifyPropertyChanged(); } }

        public new static string[] FillableFields = { "name" };

        protected override string[] GetFillableFields()
        {
            return FillableFields;
        }
    }
}
