﻿using HomeWork_11_7.Data;
using HomeWork_11_7.Users;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_11_7
{
    public class Repository<T>
        where T : FillableRecord, new()
    {
        private int maxId;
        private string path;
        private ObservableCollection<T> data;

        public Repository(string path)
        {
            data = new ObservableCollection<T>();
            maxId = 0;
            this.path = path;
            Load();         
        }

        private int NextId()
        {
            return ++maxId;
        }

        private void Load()
        {            
            if (File.Exists(path))
            {
                string[] lines = File.ReadAllLines(path);
                foreach (string line in lines)
                {
                    T t = JsonConvert.DeserializeObject<T>(line);
                    if (maxId < t.Id)
                    {
                        maxId = t.Id;
                    }

                    data.Add(t);                    
                }
                
            }
            else {
                Console.WriteLine("Database not exists!");
                File.Create(path).Close();
                Console.WriteLine("New database was created.");                
            }
        }

        public int Count()
        {
            return data.Count;
        }

        public ObservableCollection<T> All()
        {
            return data;
        }        

        public void Add(Consultant user, Dictionary<string, string> fillableData)
        {            
            T t = new T();
            t.fill(NextId(), fillableData);
            t.SetMetaData(user.GetType().Name, "created");
            data.Add(t);
            
            string[] lines = new string[1];
            lines[0] = JsonConvert.SerializeObject(t);
            File.AppendAllLines(path, lines);
        }

        public void UpdateById(Consultant user, int id, Dictionary<string, string> newData)
        {
            T t = GetById(id);
            t.updateData(newData);
            t.SetMetaData(user.GetType().Name, "updated");
            saveData();
        }

        public T GetById(int id)
        {            
            T t = data.Where(item => item.Id == id).First();

            if (t == null)
            {
                throw new Exception("Not found");
            }           

            return t;
        }

        private void saveData()
        {
            string[] lines = new string[data.Count];
            int index = 0;
            foreach (T t in data)
            {
                lines[index] = JsonConvert.SerializeObject(t);
                index++;
            }

            File.WriteAllLines(path, lines);
        }
    }
}
