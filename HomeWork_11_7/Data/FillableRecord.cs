﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_11_7.Data
{
    public abstract class FillableRecord : INotifyPropertyChanged
    {
        [JsonProperty("id")]
        protected int id;

        [JsonProperty("modifiedAt")]
        private DateTime modifiedAt;
        [JsonProperty("modifiedUserType")]
        private string modifiedUserType;
        [JsonProperty("modifiedType")]
        private string modifiedType;

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public static string[] FillableFields = { };

        [JsonIgnore]
        public int Id { get { return id; } set { } }

        protected virtual string[] GetFillableFields()
        {
            return FillableFields;
        }

        public FillableRecord()
        {
            id = 0;
        }

        public FillableRecord(int id, Dictionary<string, string> fillableData)
        {
            fill(id, fillableData);
        }

        public void fill(int id, Dictionary<string, string> fillableData)
        {
            this.id = id;
            foreach (string field in GetFillableFields())
            {
                if (fillableData[field] != null)
                {
                    FieldInfo fi = GetType().GetField(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    fi.SetValue(this, fillableData[field]);
                }
            }
        }

        public Dictionary<string, string> PrintableData()
        {
            Dictionary<string, string> data = new Dictionary<string, string>()
            {
                { "id", id.ToString() },
            };

            foreach (string field in GetFillableFields())
            {
                FieldInfo fi = GetType().GetField(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                data.Add(field, fi.GetValue(this).ToString());
            }

            return data;
        }

        public void updateData(Dictionary<string, string> newData)
        {
            foreach (KeyValuePair<string, string> pair in newData)
            {
                if (pair.Value != null && pair.Value != "")
                {
                    FieldInfo fi = GetType().GetField(pair.Key, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    fi.SetValue(this, pair.Value);
                }
            }
        }

        public void PrintMetaData()
        {
            Console.WriteLine($"{modifiedType} by {modifiedUserType} at {modifiedAt}");
        }

        public void SetMetaData(string modifiedUserType, string modifiedType)
        {
            modifiedAt = DateTime.Now;
            this.modifiedUserType = modifiedUserType;
            this.modifiedType = modifiedType;
        }
    }
}
