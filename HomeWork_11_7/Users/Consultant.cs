﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using HomeWork_11_7.Data;

namespace HomeWork_11_7.Users
{
    public class Consultant //: IClientEditable
    {
        protected Repository<Client> repositoryClient;
        protected Repository<Department> repositoryDepartment;

        public Consultant(Repository<Client> repositoryClient, Repository<Department> repositoryDepartment)
        {
            this.repositoryClient = repositoryClient;
            this.repositoryDepartment = repositoryDepartment;
        }

        public virtual string[] GetClientFillableFields()
        {
            return Client.FillableFields.Intersect(new string[] { "phoneNumber" }).ToArray();
        }

        public string getDepartmentName(int departmentId)
        {
            return repositoryDepartment.GetById(departmentId).Name;
        }

        public ObservableCollection<Department> GetDepartmentList()
        {
            return repositoryDepartment.All();
        }

        public ObservableCollection<Client> GetClientList()
        {
            return repositoryClient.All();
        }

        public ObservableCollection<Client> GetClientList(string departmentId)
        {
            return new ObservableCollection<Client>(repositoryClient.All().Where(client => client.DepartmentId == departmentId));            
        }

        public void UpdateClient(int id, Dictionary<string, string> data)
        {
            repositoryClient.UpdateById(this, id, data);
        }
    }
}
