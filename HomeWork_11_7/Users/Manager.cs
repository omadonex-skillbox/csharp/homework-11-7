﻿using HomeWork_11_7.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_11_7.Users
{
    public class Manager : Consultant //, IClientCreatable
    {
        public Manager(Repository<Client> repositoryClient, Repository<Department> repositoryDepartment) :
            base(repositoryClient, repositoryDepartment)
        {
        }

        public void AddDepartment(string name)
        {
            Dictionary<string, string> data = new Dictionary<string, string> ()
            {
                { "name", name },
            };            
            repositoryDepartment.Add(this, data);            
        }

        public void UpdateDepartment(int id, string name)
        {
            Dictionary<string, string> data = new Dictionary<string, string>()
            {
                { "name", name },
            };

            repositoryDepartment.UpdateById(this, id, data);
        }

        public override string[] GetClientFillableFields()
        {
            return Client.FillableFields.Intersect(
                new string[] { "firstName", "lastName", "optName", "passportSeries", "passportNumber", "phoneNumber", "departmentId" }
                ).ToArray();
        }

        public void AddClient(Dictionary<string, string> data)
        {
            repositoryClient.Add(this, data);
        }
    }
}