﻿using HomeWork_11_7.Data;
using HomeWork_11_7.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HomeWork_11_7
{
    /// <summary>
    /// Interaction logic for WindowClient.xaml
    /// </summary>
    public partial class WindowClient : Window
    {
        private WindowMain parent;
        private Client? client;
        public WindowClient(WindowMain parent, Client? client = null)
        {
            InitializeComponent();
            this.parent = parent;
            this.client = client;
        }

        private void ButtonAction_Click(object sender, RoutedEventArgs e)
        {
            if (ComboBoxDepartment.SelectedIndex == -1)
            {
                MessageBox.Show("Fill all fields please");
            } 
            else
            {
                Dictionary<string, string> dict = new Dictionary<string, string>()
                {
                    { "firstName", TextBoxFirstName.Text },
                    { "lastName", TextBoxLastName.Text },
                    { "optName", TextBoxOptName.Text },
                    { "passportSeries", TextBoxPassportSeries.Text },
                    { "passportNumber", TextBoxPassportNumber.Text },
                    { "phoneNumber", TextBoxPhoneNumber.Text },
                    { "departmentId", ComboBoxDepartment.SelectedValue.ToString() },
                };

                bool pass = true;
                foreach (string key in dict.Keys)
                {
                    if ((dict[key] == null) || (dict[key] == ""))
                    {
                        pass = false;
                        break;
                    }
                }

                if (pass)
                {
                    if (client != null)
                    {
                        parent.User.UpdateClient(client.Id, dict);                        
                    }
                    else
                    {
                        (parent.User as Manager).AddClient(dict);
                    }
                    Close();
                }
                else
                {
                    MessageBox.Show("Fill all fields please");
                }
            }                       
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            ComboBoxDepartment.ItemsSource = parent.User.GetDepartmentList();

            if (client == null)
            {
                Title = "Add client";
                ButtonAction.Content = "Add";
            }
            else
            {
                Title = "Edit client";
                ButtonAction.Content = "Save";
                TextBoxFirstName.Text = client.FirstName;                
                TextBoxLastName.Text = client.LastName;
                TextBoxOptName.Text = client.OptName;
                TextBoxPassportNumber.Text = client.PassportNumber;
                TextBoxPassportSeries.Text = client.PassportSeries;
                TextBoxPhoneNumber.Text = client.PhoneNumber;
                ComboBoxDepartment.SelectedValue = client.DepartmentId;

                if (parent.Mode == WindowMain.ModeConsultant) 
                { 
                    TextBoxFirstName.IsReadOnly = true;
                    TextBoxLastName.IsReadOnly = true;
                    TextBoxOptName.IsReadOnly = true;
                    TextBoxPassportNumber.IsReadOnly = true;
                    TextBoxPassportSeries.IsReadOnly = true;
                    ComboBoxDepartment.IsReadOnly = true;
                }
            }
        }
    }
}
