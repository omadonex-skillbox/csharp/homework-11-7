﻿using HomeWork_11_7.Data;
using HomeWork_11_7.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HomeWork_11_7
{
    /// <summary>
    /// Interaction logic for WindowMain.xaml
    /// </summary>
    public partial class WindowMain : Window
    {
        public const string PathDataClient = "dataClient.txt";
        public const string PathDataDepartment = "dataDepartment.txt";
        public const int ModeConsultant = 1;
        public const int ModeManager = 2;
        public int Mode { get; set; }        
        public Consultant User { get; set; }
        public WindowMain()
        {
            InitializeComponent();
            WindowLogin login = new WindowLogin(this);
            login.ShowDialog();
        }

        private void ButtonAddDepartment_Click(object sender, RoutedEventArgs e)
        {
            WindowDepartment wDep = new WindowDepartment(this);
            wDep.ShowDialog();
        }

        private void ButtonAddClient_Click(object sender, RoutedEventArgs e)
        {
            WindowClient wClient = new WindowClient(this);
            wClient.ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (Mode == ModeConsultant)
            {
                User = new Consultant(new Repository<Data.Client>(PathDataClient), new Repository<Data.Department>(PathDataDepartment));
                ButtonAddDepartment.Visibility = Visibility.Hidden;
                ButtonAddClient.Visibility = Visibility.Hidden;
            } else
            {
                User = new Manager(new Repository<Data.Client>(PathDataClient), new Repository<Data.Department>(PathDataDepartment));
            }

            DataGridDepartment.ItemsSource = User.GetDepartmentList();
            DataGridClient.ItemsSource = User.GetClientList();
        }

        private void DataGridDepartment_RowDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            WindowDepartment wDep = new WindowDepartment(this, row.Item as Department);
            wDep.ShowDialog();
        }

        private void DataGridClient_RowDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            WindowClient wClient = new WindowClient(this, row.Item as Client);
            wClient.ShowDialog();
        }

        private void DataGridDepartment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Department dep = (sender as DataGrid).SelectedItem as Department;
            DataGridClient.ItemsSource = User.GetClientList(dep.Id.ToString());
        }
    }
}
